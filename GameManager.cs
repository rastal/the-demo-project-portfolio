﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    
    public enum GameState {
        Starting,
        LookUnlocked,
        PlayerLooked,
        MoveUnlocked,
        PlayerMoved,
        JumpUnlocked,
        Debug
    }

    public GameObject DebugText;
    public GameObject Message;
    public GameObject Player;
    public Transform StartingCheckpoint;
    public GameState ThisGameState = GameState.Starting;
    [HideInInspector] public bool WaitingForInput = false;
    [HideInInspector] public bool WaitingForVoice = true;
    CustomDebug _debug;
    DisembodiedVoice _voice;
    PlayerController _playerController;

    void OnEnable() {
        _debug = DebugText.GetComponent<CustomDebug>();
        _voice = Message.GetComponent<DisembodiedVoice>();
        _playerController = Player.GetComponent<PlayerController>();
        GameStateChange();
    }

    void Start() {
        /*if (StartingCheckpoint != null) {
            _playerController.enabled = false;
            Player.transform.position = StartingCheckpoint.position;
            Player.transform.rotation = StartingCheckpoint.rotation;
            _playerController.enabled = true;
        }*/
    }

    void Update() {
        //_debug.Display("Current Game State", ThisGameState.ToString());
        //_debug.Display("Waiting for Input", WaitingForInput.ToString());
    }

    void GameStateChange() {
        switch (ThisGameState) {
            case GameState.Starting:
                _playerController.LookUnlocked = false;
                _playerController.MoveUnlocked = false;
                _playerController.JumpUnlocked = false;
                _playerController.RunUnlocked = false;
                WaitingForInput = false;
                WaitingForVoice = true;
                _voice.MessageList.Clear();
                _voice.MessageList.AddRange(_voice.AllMessages[0]);
                break;
            case GameState.LookUnlocked:
                _playerController.LookUnlocked = true;
                WaitingForInput = true;
                _voice.MessageList.Clear();
                _voice.MessageList.AddRange(_voice.AllMessages[1]);
                break;
            case GameState.PlayerLooked:
                WaitingForVoice = true;
                _voice.MessageList.Clear();
                _voice.MessageList.AddRange(_voice.AllMessages[2]);
                break;
            case GameState.MoveUnlocked:
                _playerController.LookUnlocked = true;
                _playerController.MoveUnlocked = true;
                WaitingForInput = true;
                _voice.MessageList.Clear();
                _voice.MessageList.AddRange(_voice.AllMessages[3]);
                break;
            case GameState.PlayerMoved:
                WaitingForVoice = true;
                _voice.MessageList.Clear();
                _voice.MessageList.AddRange(_voice.AllMessages[4]);
                break;
            case GameState.JumpUnlocked:
                _playerController.LookUnlocked = true;
                _playerController.MoveUnlocked = true;
                _playerController.JumpUnlocked = true;
                _voice.MessageList.Clear();
                break;
            case GameState.Debug:
                _playerController.LookUnlocked = true;
                _playerController.MoveUnlocked = true;
                _playerController.JumpUnlocked = true;
                _playerController.RunUnlocked = true;
                _voice.MessageList.Clear();
                break;
        }
    }

    public void VoiceComplete() {
        if (WaitingForVoice) {
            WaitingForVoice = false;
            ++ThisGameState;
            GameStateChange();
        }
    }

    public void InputComplete() {
        if (WaitingForInput) {
            WaitingForInput = false;
            ++ThisGameState;
            GameStateChange();
        }
    }
}
