﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TimeTracker : MonoBehaviour {

    [ReadOnly] public float DayTime = 0f;
    [SerializeField] float _dayLength = 30f;
    CustomDebug _debug;

    void OnEnable() {
        if (GameObject.Find("Debug Text") != null) {
            _debug = GameObject.Find("Debug Text").GetComponent<CustomDebug>();
        }
    }

    void Update() {
        DayTime = Time.time % _dayLength;

        if (_debug != null) {
            _debug.Display("Unity Time", Time.time.ToString());
            _debug.Display("Schedule Time", DayTime.ToString());
        }
    }
}
