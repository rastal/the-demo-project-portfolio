﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "ScheduleProbabilityMap", menuName = "Rastal/Schedule Probability Map")]
public class ScheduleProbabilityMap : SerializedScriptableObject {

    [DictionaryDrawerSettings(KeyLabel = "Time", ValueLabel = "Map of Location : Probability")]
    [SerializeField] Dictionary<float, Dictionary<Transform, int>> _scheduleProbabilityMap = new Dictionary<float, Dictionary<Transform, int>>();

}
