﻿# pragma warning disable 649

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using Sirenix.OdinInspector;

public class SmartLocation : MonoBehaviour {
    Queue<GameObject> _transformPool = new Queue<GameObject>();
    [SerializeField] bool _office = false;
    [SerializeField] AnimationCurve _meetingChance;
    [SerializeField] TimeTracker _timeTracker;
    bool _runMeeting = false;

    void OnEnable() {
        var emptyObject = new GameObject();
        for (var i = 0; i < 5; ++i) {
            var poolObject = GameObject.Instantiate(emptyObject, this.transform.position, Quaternion.identity, this.transform);
            _transformPool.Enqueue(poolObject);
        }
    }

    void Update() {
        if (_timeTracker.DayTime <= 0.5f && _office) {
            var meetingFloat = _meetingChance.Evaluate(Random.value);
            if (meetingFloat > 0.5f) {
                _runMeeting = true;
            } else {
                _runMeeting = false;
            }
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == 11) {
            var destinationSetter = other.GetComponent<AIDestinationSetter>();
            
            if (!_office) {
                var collider = this.GetComponent<BoxCollider>();

                var xMinimum = this.transform.position.x - (collider.size.x / 2f);
                var xMaximum = this.transform.position.x + (collider.size.x / 2f);
                var zMinimum = this.transform.position.z - (collider.size.z / 2f);
                var zMaximum = this.transform.position.z + (collider.size.z / 2f);
                
                var x = Random.Range(xMinimum, xMaximum);
                var z = Random.Range(zMinimum, zMaximum);
                
                var target = _transformPool.Dequeue();
                target.transform.position = new Vector3(x, this.transform.position.y, z);
                destinationSetter.target = target.transform;
                
                _transformPool.Enqueue(target);
            } else {
                Transform target = null;

                if (_runMeeting) {
                    target = GameObject.Find("meeting room").transform;
                } else {
                    if (other.gameObject.name == "First Lil' Capsule on a Schedule") {
                        target = GameObject.Find("head office").transform;
                    } else {
                        target = GameObject.Find("cubicles").transform;
                    }
                }
                
                destinationSetter.target = target;
            }
        } 
    }
}
