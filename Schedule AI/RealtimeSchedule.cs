﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "RealtimeSchedule", menuName = "Rastal/Realtime Schedule")]
public class RealtimeSchedule : SerializedScriptableObject {
    
    [Space, DictionaryDrawerSettings(KeyLabel = "Time", ValueLabel = "Location")]
    public Dictionary<float, Transform> Schedule = new Dictionary<float, Transform>();
}
