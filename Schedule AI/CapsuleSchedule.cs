﻿#pragma warning disable 649
#pragma warning disable 414

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using Rastal;
using Sirenix.OdinInspector;

public class CapsuleSchedule : SerializedMonoBehaviour {

    [SerializeField] TimeTracker _timeTracker;

    [Space]
    [SerializeField] float _moveSpeed;
    [SerializeField, Range(0f, 10f)] float _punctuality;
    [SerializeField, Range(0f, 10f)] float _flexibility;
    
    [Space]
    [SerializeField] RealtimeSchedule _realtimeSchedule;

    [Space]
    [SerializeField] ScheduleProbabilityMap _scheduleProbabilityMap;
    [SerializeField] Dictionary<float, Dictionary<Transform, int>> _oldScheduleProbabilityMap = new Dictionary<float, Dictionary<Transform, int>>();

    CustomDebug _debug;
    CharacterController _characterController;
    AIPath _path;
    Seeker _seeker;
    AIDestinationSetter _destinationSetter;
    List<float> _scheduleTimes = new List<float>();

    [ShowInInspector, ReadOnly, Space] Transform _destination;
    Path _nextPath = null;
    int _nextTimeIndex = 0;
    [ShowInInspector, ReadOnly] float _nextScheduleTriggerTime = 100f;  // Just trying to make sure the bots don't run right away.

    float _travelStartTime = 0f;
    float _predictedTravelTime;
    [ShowInInspector, ReadOnly] float _travelTimePredictionModifier = 0.1f;

    [ShowInInspector, ReadOnly] bool _enRoute = false;

    void OnEnable() {
        if (GameObject.Find("Debug Text") != null) {
            _debug = GameObject.Find("Debug Text").GetComponent<CustomDebug>();
        }
        _characterController = this.GetComponent<CharacterController>();
        _destinationSetter = this.GetComponent<AIDestinationSetter>();
        _path = this.GetComponent<AIPath>();
        _seeker = this.GetComponent<Seeker>();
    }

    void Start() {
        foreach (var time in _realtimeSchedule.Schedule.Keys) {
            _scheduleTimes.Add(time);
        }
        _scheduleTimes.Sort();
        _path.maxSpeed = _moveSpeed;
    }

    void Update() {
        // Compare actual travel time with predicted travel time (used for debugging only).
        if (!_path.reachedDestination && _travelStartTime == 0f && _path.hasPath) {
            _travelStartTime = Time.time;
            Debug.Log(this.name + " (punctuality " + _punctuality + ") leaving at " + _timeTracker.DayTime + " for event at " + _scheduleTimes[_nextTimeIndex - 1]);
        } else if (_path.reachedDestination && _travelStartTime != 0f) {
            _travelStartTime = 0f;
        }

        // Reset the schedule index for a new day.
        if (_timeTracker.DayTime <= 0.5f && _nextTimeIndex != 0) {
            _nextTimeIndex = 0;
            //Debug.Log("next time index: " + _nextTimeIndex);
        }

        // Caclulate the next schedule path and time.
        if (_nextPath == null) {
            if (!_realtimeSchedule.Schedule.TryGetValue(_scheduleTimes[_nextTimeIndex % _scheduleTimes.Count], out _destination)) {
                Debug.LogError($"Next destination for {this.gameObject.name} not retrieved.");
            }
            if (_destination != null) {
                NextScheduledPath(_destination);
            } else {
                Debug.LogError($"Next destination for {this.gameObject.name} not valid.");
            }
        }
        
        // Set a new pathfinding destination based on the time of day.
        if (_timeTracker.DayTime >= _nextScheduleTriggerTime && !_enRoute && _nextTimeIndex < _scheduleTimes.Count) {
            _destinationSetter.target = _destination;
            
            _destination = null;
            _nextPath = null;
            _enRoute = true;

            ++_nextTimeIndex;
        }
    }

    void PickNextDestination() {
        // Get a weighted random destination from the probability map.
        if (_oldScheduleProbabilityMap.TryGetValue(_scheduleTimes[_nextTimeIndex % _scheduleTimes.Count], out var probabilityMap)) {
            var probabilities = new List<int>();
            var destinations = new List<Transform>();

            foreach (var destination in probabilityMap.Keys) {
                destinations.Add(destination);
                if (probabilityMap.TryGetValue(destination, out var probability)) {
                    probabilities.Add(probability);
                }
            }
            var choice = new WeightedChoice(probabilities).NextValue();
            _destination = destinations[choice];
        }
    }

    void NextScheduledPath(Transform destination) {
        Vector3 startPosition = (_destinationSetter.target == null) ? this.transform.position : _destinationSetter.target.position;

        var path = ABPath.Construct(startPosition, destination.position, OnPathComplete);
        AstarPath.StartPath(path);
    }

    void OnPathComplete(Path path) {
        _seeker.PostProcess(path);
        _nextPath = path;

        // Predict travel time to next destination based on path length and movement speed
        // (used for schedule utility considerations)
        _predictedTravelTime = _nextPath.GetTotalLength() / _moveSpeed;
        _predictedTravelTime *= 1 + _travelTimePredictionModifier;
        _nextScheduleTriggerTime = _scheduleTimes[_nextTimeIndex % _scheduleTimes.Count] - _predictedTravelTime;

        // Adjust trigger time based on punctuality (and vary the adjustment randomly).
        var normalizedPunctuality = _punctuality / 10f;  // 10f is a magic number for the maximum punctuality value;
                                                         // eventually this will be encoded in a scriptable object.
        normalizedPunctuality -= Random.value;  // So the average punctuality values straddle the axis.
        normalizedPunctuality *= -1f;  // So higher punctuality brings the trigger time sooner.

        _nextScheduleTriggerTime += normalizedPunctuality * 3f;  // 3f is a magic number denoting the range of possible
                                                                 // schedule time variations; need to do something cleaner
                                                                 // and more permanent eventually.
        
        _enRoute = false;
    }
}
