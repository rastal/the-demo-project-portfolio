﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Rastal {
    public class WeightedChoice {
        
        // Initializes a new loaded die. probabilities
        // is an array of numbers indicating the relative
        // probability of each choice relative to all the
        // others.  For example, if probabilities is [3,4,2], then
        // the chances are 3/9, 4/9, and 2/9, since the probabilities
        // add up to 9.

        // Usage example:
        // var weightedChoice = new WeightedChoice(new int[] {150, 40, 15, 3});  // list of probabilities for each number:
                                                                                 // 0 is 150, 1 is 40, and so on
        // int number = weightedChoice.nextValue();  // return a number from 0-3 according to given probabilities;
                                                     // the number can be an index to another array, if needed
        
        System.Random _random = new System.Random();
        
        List<long> _probabilities;
        List<int> _aliases;
        long _total;
        int _probabilityCount;
        bool _even;

        public WeightedChoice(IEnumerable<int> probabilities){
            // Raise an error if nil
            if (probabilities == null) {
                throw new ArgumentNullException("probabilities");
            }

            this._probabilities = new List<long>();
            this._aliases = new List<int>();
            this._total = 0;
            this._even = false;
            var smalls = new List<int>();
            var larges = new List<int>();
            var tempProbabilities = new List<long>();
            foreach(var probability in probabilities) {
                tempProbabilities.Add(probability);
            }
            this._probabilityCount = tempProbabilities.Count;

            // Get the max and min choice and calculate total
            long maximum = -1, minimum = -1;
            foreach (var probability in tempProbabilities) {
                if (probability<0) {
                    throw new ArgumentException("probabilities contains a negative probability.");
                }
                maximum = (maximum < 0 || probability > maximum) ? probability : maximum;
                minimum = (minimum < 0 || probability < minimum) ? probability : minimum;
                this._total += probability;
            }

            // We use a shortcut if all probabilities are equal
            if (maximum == minimum) {
                this._even = true;
                return;
            }

            // Clone the probabilities and scale them by the number of probabilities
            for (var i = 0; i < tempProbabilities.Count; ++i) {
                tempProbabilities[i]*=this._probabilityCount;
                this._aliases.Add(0);
                this._probabilities.Add(0);
            }
            // Use Michael Vose's alias method
            for (var i = 0; i < tempProbabilities.Count; ++i) {
                if (tempProbabilities[i] < this._total) {
                    smalls.Add(i); // Smaller than probability sum
                } else {
                    larges.Add(i); // Probability sum or greater
                }
            }
            // Calculate probabilities and aliases
            while (smalls.Count > 0 && larges.Count > 0) {
                var small = smalls[smalls.Count-1];
                smalls.RemoveAt(smalls.Count-1);
                
                var large = larges[larges.Count-1];
                larges.RemoveAt(larges.Count-1);
                
                this._probabilities[small] = tempProbabilities[small];
                this._aliases[small]=large;
                
                var newProbability = (tempProbabilities[large] + tempProbabilities[small]) - this._total;
                tempProbabilities[large] = newProbability;
                
                if(newProbability<this._total) {
                    smalls.Add(large);
                } else {
                    larges.Add(large);
                }
            }

            foreach (var large in larges) {
                this._probabilities[large] = this._total;
            }
            foreach(var small in smalls) {
                this._probabilities[small] = this._total;
            }
        }
        
        // Returns the number of choices.
        public int Count {
            get {
                return this._probabilityCount;
            }
        }

        // Chooses a choice at random, ranging from 0 to the number of choices
        // minus 1.
        public int NextValue() {
            var i = _random.Next(this._probabilityCount);
            return (this._even || _random.Next((int)this._total) < this._probabilities[i]) ? i : this._aliases[i];
        }
    }
}
