using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using NaughtyAttributes;
using Rewired;

public class PauseResume : MonoBehaviour {
    
    const float _pauseTimeScale = 0.001f;

    Player _playerInput;
    ControllerMapEnabler _mapEnabler;

    [Scene, SerializeField] string _pauseScene;

    public void Resume() {
        foreach (var ruleSet in _mapEnabler.ruleSets) {
            ruleSet.enabled = false;
        }
        _mapEnabler.ruleSets.Find(item => item.tag == "Gameplay").enabled = true;
        _mapEnabler.Apply();

        Time.timeScale = 1f;
        SceneManager.UnloadSceneAsync(_pauseScene);
    }

    void Awake() {
        _playerInput = ReInput.players.GetPlayer(0);
        _mapEnabler = _playerInput.controllers.maps.mapEnabler;
    }

    void Update() {
        if (_playerInput.GetButtonDown("Pause")) {
            Pause();
        } else if (_playerInput.GetButtonDown("Unpause")) {
            Resume();
        }
    }

    void Pause() {
        foreach (var ruleSet in _mapEnabler.ruleSets) {
            ruleSet.enabled = false;
        }
        _mapEnabler.ruleSets.Find(item => item.tag == "Menu").enabled = true;
        _mapEnabler.Apply();

        Time.timeScale = _pauseTimeScale;
        SceneManager.LoadScene(_pauseScene, LoadSceneMode.Additive);
    }
}
