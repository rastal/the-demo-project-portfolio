# The Demo Project

This started as just a test to see how easily I could make a 3D character controller in Unity that felt really snappy. It turned into a general project for experimenting with any aspect of Unity that I was interested in, from level design to AI to input to UI, while using mostly free assets.

It currently sits on itch as a small (as in 5-10 minutes of gameplay), atmospheric 3D platformer (that's playable on the WEB, d00d!). People seem to find it fun. I tend to agree.

**[Play it on itch.](https://therastal.itch.io/portfolio)**