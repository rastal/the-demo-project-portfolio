﻿#pragma warning disable 0649 // disable warnings about unused variables

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
    [SerializeField] Transform _target;
    [SerializeField] PlayerController _playerController;

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            _playerController.Teleport(_target);
        }
    }
}
