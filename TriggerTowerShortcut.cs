﻿#pragma warning disable 0649 // disable warnings about unused variables

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTowerShortcut : MonoBehaviour {

    [SerializeField] GameObject _portalBlock;
    [SerializeField] Light _portalLight;
    [SerializeField] DisembodiedVoice _message;
    bool _unlocked = false;

    void Start() {
        _portalLight.enabled = false;
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && !_unlocked) {   
            _message.DisplayNow("shortcut unlocked");
            _portalBlock.SetActive(false);
            _portalLight.enabled = true;
            _unlocked = true;
        }
    }

}
