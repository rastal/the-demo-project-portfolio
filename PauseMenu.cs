﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Cinemachine;
using Rewired;
using TMPro;

public class PauseMenu : MonoBehaviour {

    [SerializeField] AudioMixer _mixer = null;
    CustomDebug _debug;
    [SerializeField] PauseResume _pauseResume;
    Player _playerInput;
    CinemachineFreeLook _freeLook;

    GameObject _pauseMenu;
    Slider _musicSlider;
    Slider _sfxSlider;
    
    GameObject _controlsMenu;
    TMP_Dropdown _mouseXDropdown;
    TMP_Dropdown _mouseYDropdown;
    TMP_Dropdown _gamepadXDropdown;
    TMP_Dropdown _gamepadYDropdown;
    Slider _mouseSensitivitySlider;
    Slider _gamepadSensitivitySlider;

    List<ActionElementMap> _actionMaps = new List<ActionElementMap>();

    void Awake() {
        _playerInput = ReInput.players.GetPlayer(0);
    }

    void OnEnable() {
        _freeLook = GameObject.Find("Player Camera").GetComponent<CinemachineFreeLook>();
        
        _pauseMenu = GameObject.Find("Pause Menu");
        _musicSlider = GameObject.Find("Music Volume Slider").GetComponent<Slider>();
        _sfxSlider = GameObject.Find("SFX Volume Slider").GetComponent<Slider>();
        
        _controlsMenu = GameObject.Find("Controls Menu");
    }

    void Start() {
        _controlsMenu.SetActive(false);
        _mixer.GetFloat("Music Volume", out float value);
        _musicSlider.value = value;
        _mixer.GetFloat("SFX Volume", out value);
        _sfxSlider.value = value;

        var tempDebug = GameObject.Find("Debug Text");
        _debug = tempDebug.GetComponent<CustomDebug>();
    }

    //
    // Pause Menu
    //

    public void Resume() {
        _pauseResume.Resume();
    }

    public void SetMusicVolume(float slider) {
        _mixer.SetFloat("Music Volume", slider);
    }

    public void SetSFXVolume(float slider) {
        _mixer.SetFloat("SFX Volume", slider);
    }

    public void PauseToControls() {
        _pauseMenu.SetActive(false);
        _controlsMenu.SetActive(true);

        _mouseXDropdown = GameObject.Find("Camera X (mouse)").GetComponent<TMP_Dropdown>();
        _mouseYDropdown = GameObject.Find("Camera Y (mouse)").GetComponent<TMP_Dropdown>();
        _gamepadXDropdown = GameObject.Find("Camera X (gamepad)").GetComponent<TMP_Dropdown>();
        _gamepadYDropdown = GameObject.Find("Camera Y (gamepad)").GetComponent<TMP_Dropdown>();

        _mouseSensitivitySlider = GameObject.Find("Mouse Sensitivity Slider").GetComponent<Slider>();
        _mouseSensitivitySlider.value = _playerInput.controllers.maps.InputBehaviors[0].mouseXYAxisSensitivity;
        _gamepadSensitivitySlider = GameObject.Find("Gamepad Sensitivity Slider").GetComponent<Slider>();
        _gamepadSensitivitySlider.value = _playerInput.controllers.maps.InputBehaviors[0].joystickAxisSensitivity;

        EventSystem.current.SetSelectedGameObject(GameObject.Find("Camera X (mouse)"));
        
        // So the dropdowns display the correct things as selected.
        _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Mouse, "LookX", false, _actionMaps);
        if (_actionMaps.Count > 0) {
            if (_actionMaps[0].invert) {
                _mouseXDropdown.SetValueWithoutNotify(1);
            } else {
                _mouseXDropdown.SetValueWithoutNotify(0);
            }
        }
        _actionMaps.Clear();

        _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Mouse, "LookY", false, _actionMaps);
        if (_actionMaps.Count > 0) {
            if (_actionMaps[0].invert) {
                _mouseYDropdown.SetValueWithoutNotify(1);
            } else {
                _mouseYDropdown.SetValueWithoutNotify(0);
            }
        }
        _actionMaps.Clear();

        _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Joystick, "LookX", false, _actionMaps);
        if (_actionMaps.Count > 0) {
            if (_actionMaps[0].invert) {
                _gamepadXDropdown.SetValueWithoutNotify(1);
            } else {
                _gamepadXDropdown.SetValueWithoutNotify(0);
            }
        }
        _actionMaps.Clear();

        _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Joystick, "LookY", false, _actionMaps);
        if (_actionMaps.Count > 0) {
            if (_actionMaps[0].invert) {
                _gamepadYDropdown.SetValueWithoutNotify(1);
            } else {
                _gamepadYDropdown.SetValueWithoutNotify(0);
            }
        }
        _actionMaps.Clear();
    }
    
    public void Quit() {
        Application.Quit();
    }

    //
    // Controls Menu
    //

    public void SetCameraXMouse(int value) {
        if (value == 0) {
            _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Mouse, "LookX", false, _actionMaps);
            _actionMaps[0].invert = false;
        } else {
            _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Mouse, "LookX", false, _actionMaps);
            _actionMaps[0].invert = true;
        }
        _actionMaps.Clear();
    }

    public void SetCameraXGamepad(int value) {
        if (value == 0) {
            _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Joystick, "LookX", false, _actionMaps);
            _actionMaps[0].invert = false;
        } else {
            _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Joystick, "LookX", false, _actionMaps);
            _actionMaps[0].invert = true;
        }
        _actionMaps.Clear();
    }

    public void SetCameraYMouse(int value) {
        if (value == 0) {
            _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Mouse, "LookY", false, _actionMaps);
            _actionMaps[0].invert = false;
        } else {
            _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Mouse, "LookY", false, _actionMaps);
            _actionMaps[0].invert = true;
        }
        _actionMaps.Clear();
    }
    
    public void SetCameraYGamepad(int value) {
        if (value == 0) {
            _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Joystick, "LookY", false, _actionMaps);
            _actionMaps[0].invert = false;
        } else {
            _playerInput.controllers.maps.GetAxisMapsWithAction(ControllerType.Joystick, "LookY", false, _actionMaps);
            _actionMaps[0].invert = true;
        }
        _actionMaps.Clear();
    }

    public void SetMouseSensitivity(float slider) {
        _playerInput.controllers.maps.InputBehaviors[0].mouseXYAxisSensitivity = slider;
    }

    public void SetGamepadSensitivity(float slider) {
        _playerInput.controllers.maps.InputBehaviors[0].joystickAxisSensitivity = slider;
    }

    public void ControlsToPause() {
        _controlsMenu.SetActive(false);
        _pauseMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(GameObject.Find("Resume Button"));
    }
}
