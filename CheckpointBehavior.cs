﻿#pragma warning disable 0649 // disable warnings about unused variables

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointBehavior : MonoBehaviour {

    [SerializeField] Transform _checkpoint;
    Collider _collider;

    void Awake() {
        _collider = this.GetComponent<Collider>();
    }

    void OnTriggerEnter(Collider other) {
        if (other.GetComponent<PlayerController>() != null) {
            other.GetComponent<PlayerController>().Checkpoint = _checkpoint;
        }
    }
}
