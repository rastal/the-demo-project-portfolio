﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalljumpTrigger : MonoBehaviour {
    
    PlayerController _playerController;
    Collider _walljumpTrigger;

    void OnEnable() {
        _playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        _walljumpTrigger = this.GetComponent<Collider>();
    }

    void OnTriggerStay(Collider other) {
        if (other.gameObject.layer >= 8 && 
            other.gameObject.layer <= 10 &&
            !_playerController.IsGrounded) {

            _playerController.WalljumpTrigger = true;
        }
    }

    void OnTriggerExit(Collider other) {
        _playerController.WalljumpTrigger = false;
    }
}
