using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerController : MonoBehaviour {
    
    [HideInInspector] public Transform Checkpoint;
    [HideInInspector] public bool IsGrounded = false;
    [HideInInspector] public bool WalljumpTrigger = false;
    
    // Ability unlocks
    public bool LookUnlocked = true;
    public bool MoveUnlocked = true;
    public bool JumpUnlocked = true;
    public bool RunUnlocked = true;

    const float _minMoveDistance = 0.001f;
    const float _turnSmoothTime = 0.1f;
    const float _wallNormalBound = 0.35f;
    const float _wallHitDuration = 0.2f;
    const float _groundNormalBound = 0.35f;
    const int _collisionDuration = 2;  // How many FixedUpdates before not considered colliding.
    const float _walljumpVelocity = 2.5f;
    
    [SerializeField] bool _mouseEnabled = true;
    [SerializeField] float _gravityModifier = 1f;
    [SerializeField] float _moveSpeed = 9f;
    [SerializeField] float _runSpeed = 1.75f;
    [SerializeField] float _jumpVelocity = 6f;
    [SerializeField] float _fallMultiplier = 2.5f;
    [SerializeField] float _lowJumpMultiplier = 3f;
    [SerializeField] float _walljumpDuration = 0.75f;

    Player _playerInput;
    ControllerMapEnabler _mapEnabler;
    CustomDebug _debug;
    CharacterController _controller;
    Transform _mainCamera;
    GameObject _playerCamera;
    GameManager _gameManager;
    Animator _animator;
    DisembodiedVoice _message;
    Vector3 _velocity;
    Vector3 _velocityLastFrame;
    float _turnSmoothVelocity;
    float _previousAngle;
    float _playerTurn;
    bool _isColliding = false;
    int _fixedUpdatesSinceCollision = 0;
    Vector3 _currentNormal;
    bool _hitWallThisFrame = false;
    float _hitWallStartTime;
    bool _canJump = false;
    bool _jumpedThisFrame = false;
    bool _canWalljump = false;
    bool _walljumpedThisFrame = false;
    bool _isWalljumping = false;
    float _walljumpStartTime = 0f;
    Vector3 _walljumpDirection;
    bool _landedThisFrame = false;

    AudioSource _footsteps;
    AudioSource _jumping;
    AudioSource _landing;

    public void Teleport(Transform target) {
        _controller.enabled = false;
        this.transform.position = target.position;
        this.transform.rotation = target.rotation;
        _controller.enabled = true;
    }

    void Awake() {
        _playerInput = ReInput.players.GetPlayer(0);
        _mapEnabler = _playerInput.controllers.maps.mapEnabler;
    }

    void OnEnable() {
        // TODO: Update this to the Rewired system.
        /*if (_mouseEnabled) {
            InputSystem.EnableDevice(Mouse.current);
        } else {
            InputSystem.DisableDevice(Mouse.current);
        }*/
        
        _controller = this.GetComponent<CharacterController>();
        _mainCamera = GameObject.Find("Main Camera").transform;
        _playerCamera = GameObject.Find("Player Camera");
        _animator = this.GetComponentInChildren<Animator>();
        _gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        
        _footsteps = GameObject.Find("Footsteps").GetComponent<AudioSource>();
        _jumping = GameObject.Find("Jumping").GetComponent<AudioSource>();
        _landing = GameObject.Find("Landing").GetComponent<AudioSource>();
        
        var tempDebug = GameObject.Find("Debug Text");
        _debug = tempDebug.GetComponent<CustomDebug>();

        _message = GameObject.Find("Message").GetComponent<DisembodiedVoice>();
    }

    void Start() {
        Checkpoint = _gameManager.StartingCheckpoint;
    } 

    void OnControllerColliderHit(ControllerColliderHit hit) {
        
        if (hit.gameObject.layer >= 8 && hit.gameObject.layer <= 10) {
            _currentNormal = hit.normal;
            _isColliding = true;
            _fixedUpdatesSinceCollision = 0;
        }
        
        // Ground stuff
        if (_currentNormal.y <= 1 + _groundNormalBound && _currentNormal.y >= 1 - _groundNormalBound) {
            IsGrounded = true;
        }

        // Wall stuff
        if (_currentNormal.y <= _wallNormalBound && _currentNormal.y >= -_wallNormalBound) {
            _hitWallThisFrame = true;
            _hitWallStartTime = Time.time;
        }

        // Death stuff
        if (hit.collider.gameObject.layer == 12) {
            Die();
        }
    }

    void Update() {        
        // Reset collision states if not colliding with anything.
        if (_fixedUpdatesSinceCollision > _collisionDuration) {
            _isColliding = false;
            _hitWallThisFrame = false;
            IsGrounded = false;
        }
        if (Time.time - _walljumpStartTime > _walljumpDuration || 
            (_hitWallThisFrame && _currentNormal.x != _walljumpDirection.x && 
                _currentNormal.z != _walljumpDirection.z)) {
            _isWalljumping = false;
        }

        //
        // Input
        //

        // Toggle timescale (for debugging)
        if (_playerInput.GetButtonDown("ToggleTimeScale")) {
            if (Time.timeScale == 1f) {
                Time.timeScale = 0.5f;
            } else {
                Time.timeScale = 1f;
            }
        }

        // TODO: Update this to Rewired input.
        // Mouse enable / disable (for debugging).
        if (_playerInput.GetButtonDown("ToggleMouse") && _mouseEnabled) {
            _mouseEnabled = false;
            //InputSystem.DisableDevice(Mouse.current);
        } else if (_playerInput.GetButtonDown("ToggleMouse") && !_mouseEnabled) {
            _mouseEnabled = true;
            //InputSystem.EnableDevice(Mouse.current);
        }

        // Horizontal movement.
        var moveInput = new Vector2(_playerInput.GetAxis("MoveX"), _playerInput.GetAxis("MoveY"));
        var runButton = _playerInput.GetButton("Run");
        float runMult;
        if (runButton) {
            runMult = _runSpeed;
        } else {
            runMult = 1f;
        }

        // Jumping (and walljumping).
        var jumpButton = _playerInput.GetButton("Jump");
        if (!jumpButton) {
            _jumpedThisFrame = false;
            _walljumpedThisFrame = false;
            if (IsGrounded && !_canJump) {
                _canJump = true;
            }
            // Only allow walljumping once you release the jump button.
            if (!IsGrounded && Time.time - _hitWallStartTime < _wallHitDuration) {
                _canWalljump = true;
                _walljumpDirection = new Vector3(_currentNormal.x, 0f, _currentNormal.z);
            }
            if ((_canWalljump && !WalljumpTrigger) || IsGrounded) {
                _canWalljump = false;
            }
        } else if (jumpButton) {
            if (!_jumpedThisFrame && IsGrounded && _canJump) {
                _jumpedThisFrame = true;
                // Don't allow continuous hopping by holding down the button.
                _canJump = false;
                _canWalljump = false;
            } else if (!_walljumpedThisFrame && _canWalljump) {
                _walljumpedThisFrame = true;
                _isWalljumping = true;
                _walljumpStartTime = Time.time;
                _canWalljump = false;
            } else {
                // Only flag these during the first frame of jumping.
                _jumpedThisFrame = false;
                _walljumpedThisFrame = false;
            }
        }

        //
        // Movement
        //
        
        var moveInputInfluence = (Time.time - _walljumpStartTime) / _walljumpDuration;
        var walljumpInfluence = 1 - moveInputInfluence;
        var direction = new Vector3(moveInput.x, 0f, moveInput.y).normalized;
        // TODO: make it so that just jumping into a wall doesn't stop movement completely,
        //       only intentionally jumping into a wall does
        if (!IsGrounded && Time.time - _hitWallStartTime < _wallHitDuration) {
            direction = Vector3.zero;
        }
        
        // Rotate the character in the direction of movement and move in that direction
        if ((direction.magnitude >= _minMoveDistance || _isWalljumping)) {
            var targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + _mainCamera.eulerAngles.y;
            var angle = 0f;
            if (!_isWalljumping) {
                angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle,
                                              ref _turnSmoothVelocity, _turnSmoothTime);
            } else {
                angle = Mathf.Atan2(_walljumpDirection.x, _walljumpDirection.z) * Mathf.Rad2Deg;
            }
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            if (angle - _previousAngle > 300f) {
                angle -= 360f;
            } else if (angle - _previousAngle < -300f) {
                angle += 360f;
            }
            _playerTurn = angle - _previousAngle;
            _previousAngle = angle;

            var moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            var move = new Vector3();
            if (_isWalljumping) {
                move = moveDirection.normalized * runMult * _moveSpeed * Time.deltaTime * moveInput.magnitude * moveInputInfluence;
                move += _walljumpDirection.normalized * _jumpVelocity * _walljumpVelocity * walljumpInfluence * Time.deltaTime;
            } else {
                move = moveDirection.normalized * runMult * _moveSpeed * Time.deltaTime * moveInput.magnitude;
            }
            _controller.Move(move);
        }

        // TODO: account for upward slopes in jumping (make character jump 
        //       higher based on slope angle).

        // Apply upward jump velocity if jumped or walljumped this frame.
        if (_jumpedThisFrame || _walljumpedThisFrame) {
            _velocity = Vector3.up * _jumpVelocity;
            IsGrounded = false;
        }

        // Apply normal gravity.
        _velocity += _gravityModifier * Physics.gravity * Time.deltaTime;

        // Change vertical velocity based on grounded vs jumping vs falling
        // (combo of Brackeys' first person movement tutorial, Board to Bits'
        // "better jumping with four lines of code" tutorial, and my own stuff).
        if (!IsGrounded && _velocity.y < 0f) {
            _velocity += Vector3.up * Physics.gravity.y * (_fallMultiplier - 1) * Time.deltaTime;
        } else if (IsGrounded && _velocity.y < 0f) {
            _velocity = new Vector3(0f, -1f, 0f);
            _hitWallThisFrame = false;
        } else if (_velocity.y > 0f && !jumpButton && !_isWalljumping) {
            _velocity += Vector3.up * Physics.gravity.y * (_lowJumpMultiplier - 1) * Time.deltaTime;
        } else if (!_isColliding) {
            _velocity = new Vector3(0f, _velocity.y, 0f);
        }

        // Slide down sloped walls.
        if (_hitWallThisFrame && _currentNormal.y > 0f) {
            Vector3 wallPush = new Vector3(_currentNormal.x, 0f, _currentNormal.z);
            // The direction the wall is sloping * delta time * how fast they should be 
            // moving downward (except turned sideways, and positive) * how steep the slope is.
            _velocity += wallPush * Time.deltaTime * -_velocity.y * (1 - _currentNormal.y);
        }
        
        _controller.Move(_velocity * Time.deltaTime);

        if (_landedThisFrame) {
            _landedThisFrame = false;
        } else if (_velocityLastFrame.y < -3f && _velocity.y == -2f) {
            _landedThisFrame = true;
        }
        _velocityLastFrame = _velocity;

        //
        // Custom Debug stuff
        //

        _debug.Display("Position", this.transform.position.ToString());
        //_debug.Display("Is Colliding", _isColliding.ToString());
        //_debug.Display("Is Grounded", IsGrounded.ToString());
        //_debug.Display("Direction", direction.ToString());
        //_debug.Display("Direction Magnitude", direction.magnitude.ToString());
        _debug.Display("Hit Wall this Frame", _hitWallThisFrame.ToString());
        _debug.Display("Wall Jump Trigger", WalljumpTrigger.ToString());
        //_debug.Display("Can Jump", _canJump.ToString());
        _debug.Display("Can Wall Jump", _canWalljump.ToString());
        _debug.Display("Wall Hit Start Time", _hitWallStartTime.ToString());
        _debug.Display("Hitting Wall", (Time.time - _hitWallStartTime < _wallHitDuration).ToString());
        _debug.Display("Is Wall Jumping", _isWalljumping.ToString());
        _debug.Display("Current Normal", _currentNormal.ToString());
        //_debug.Display("Velocity", _velocity.ToString());
        //_debug.Display("Checkpoint ID", Checkpoint.ID.ToString());

        //
        // Animator Controller stuff
        //

        _animator.SetBool("Is Grounded", IsGrounded);
        if (_jumpedThisFrame || _walljumpedThisFrame) {
            _animator.SetTrigger("Jump");
        } else {
            _animator.ResetTrigger("Jump");
        }
        _animator.SetBool("Wall Jumped This Frame", _walljumpedThisFrame);
        _animator.SetFloat("Vertical Speed", _velocity.y);
        _animator.SetFloat("Move Speed", moveInput.magnitude * runMult);
        _animator.SetFloat("Player Turn", _playerTurn);

        //
        // Audio stuff
        //

        if (_jumpedThisFrame || _walljumpedThisFrame) {
            PlaySound(_jumping);
        }
    }

    // TODO: just put this in the Update loop and base it off seconds
    void FixedUpdate() {
        ++_fixedUpdatesSinceCollision;
    }

    void Die() {
        _controller.enabled = false;
        this.transform.position = Checkpoint.position;
        this.transform.localRotation = Checkpoint.rotation;
        _controller.enabled = true;
        _message.DisplayNow("haha you died");
    }

    void PlaySound(AudioSource sound) {
        var soundVariation = Random.Range(0f, 0.05f);
        var originalPitch = sound.pitch;
        var originalVolume = sound.volume;
        
        sound.pitch -= soundVariation;
        sound.volume -= soundVariation / 2f;
        
        sound.Play();
        
        sound.pitch = originalVolume;
        sound.volume = originalVolume;
    }
}