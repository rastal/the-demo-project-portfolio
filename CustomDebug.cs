﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CustomDebug : MonoBehaviour {

    [SerializeField] bool _debugMode = true;
    Dictionary<string, string> _debugDictionary = new Dictionary<string, string>();
    TextMeshProUGUI _textMeshPro;

    void Awake() {
        _textMeshPro = GetComponent<TextMeshProUGUI>();
    }

    void Update() {
        _textMeshPro.text = "";
        foreach (KeyValuePair<string, string> entry in _debugDictionary) {
            _textMeshPro.text += entry.Key + ": " + entry.Value + "\n";
        }
    }

    public void Display(string name, string value) {
        if (_debugMode) {
            _debugDictionary[name] = value;
        }
    }
}
