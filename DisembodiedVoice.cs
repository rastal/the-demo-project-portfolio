﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;

public class DisembodiedVoice : SerializedMonoBehaviour {
    
    [HideInInspector] public List<string> MessageList = new List<string>();
    public List<List<string>> AllMessages = new List<List<string>>();
    const float _messageBuffer = 0.5f;
    const float _displayDuration = 1f;
    const float _fadeDuration = 1f;
    TextMeshProUGUI _message;
    float _nextMessageTime;
    bool _messageDisplayed = false;
    PlayerController _playerController;
    CustomDebug _debug;
    GameManager _gameManager;
    
    void OnEnable() {
        _message = this.gameObject.GetComponent<TextMeshProUGUI>();
        _message.text = "";

        _playerController = GameObject.Find("Player").GetComponent<PlayerController>();

        _debug = GameObject.Find("Debug Text").GetComponent<CustomDebug>();
        _gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }

    void Start() {
        if (_gameManager.ThisGameState == GameManager.GameState.Starting) {
            _nextMessageTime = 3.5f;
        }
    }

    void Update() {
        _debug.Display("Time.time", Time.time.ToString());
        _debug.Display("Next Message Time", _nextMessageTime.ToString());
        _debug.Display("Message Alpha", _message.alpha.ToString());

        if (!_messageDisplayed && Time.time > _nextMessageTime && MessageList.Count > 0) {
            DisplayNext();
        }
        if (MessageList.Count == 0 && _gameManager.WaitingForVoice) {
            _gameManager.VoiceComplete();
        } 
    }

    void DisplayNext() {
        var messageText = MessageList[0];
        MessageList.Remove(MessageList[0]);
        DisplayNow(messageText);
    }

    public void DisplayNow(string messageText) {
        _messageDisplayed = true;
        _message.alpha = 1f;
        _message.text = messageText;
        DOTween.To(()=> _message.alpha, x=> _message.alpha = x, 0f, _fadeDuration)
            .SetDelay(_displayDuration)
            .SetEase(Ease.InCubic)
            .OnComplete(Done);
    }

    void Done() {
        _messageDisplayed = false;
        _message.text = "";
        _nextMessageTime = Time.time + _messageBuffer;
    }
}
