﻿#pragma warning disable 0649 // disable warnings about unused variables

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerExitText : MonoBehaviour {
    
    [SerializeField] Canvas _exitTextCanvas;
    bool _unlocked = false;

    void Start() {
        _exitTextCanvas.enabled = false;
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && !_unlocked) {
            _exitTextCanvas.enabled = true;
            _unlocked = true;
        }
    }

}
