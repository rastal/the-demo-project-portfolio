﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BackgroundMusic : MonoBehaviour {
    const float _musicLength = 192f;
    CustomDebug _debug;
    float _startTime = -300f;
    float _quietTime = 300f;
    float _fadeOutTime = 30f;
    bool _canFadeOut = false;
    AudioSource _music;


    void Awake() {
        _music = GetComponent<AudioSource>();

        var tempDebug = GameObject.Find("Debug Text");
        _debug = tempDebug.GetComponent<CustomDebug>();

        DOTween.Init(true, true);
    }

    void Update() {
        if (Time.time - _startTime > _quietTime &&
            !_music.isPlaying) {
                
            _music.volume = 0f;
            _music.Play();
            _music.DOFade(.5f, 10f);
            _startTime = Time.time;
            _canFadeOut = true;
            
        } else if (_music.isPlaying && Time.time - _startTime > _musicLength - _fadeOutTime &&
                   _canFadeOut) {
            
            _music.DOFade(0f, 10f);
            _canFadeOut = false;
        }

        _debug.Display("Music is Playing", _music.isPlaying.ToString());
        _debug.Display("Music Volume", _music.volume.ToString());
        _debug.Display("Total Playing Tweens", DOTween.TotalPlayingTweens().ToString());
    }
}
